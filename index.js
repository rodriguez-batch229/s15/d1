// JS Comments ctrl+/
// They are important, acts as guide for the programmer
// Comments are disregarded no matter how long it may be.

/* Multiline comment alt+shift+A */

// Statements
// Programming instruction that we tell the computer to perform.
// Statements usually ends with semicolon (;)

// Syntax
//  it is the set of rules that describes how the statements must be constructed

// alert("hello World");

console.log("hello World");

// JS is a loose type programming language
console.log("Hello Again");

// [Section] Variables
// It is used as a container or storage

// Declaring Variables - Tells our device that a variable name is created and is ready to store data

// Syntax - let/const variableName;

// "let" is a keyword that is usually used to declare a variable

let myVariable;
let hello;

console.log(myVariable);
console.log(hello);

// Guidelines for declaring a variable
/*
1. use let keyword follwed by a variable name and then followed again by the assignment
2. varaiable names should start with a lowercase letter
3. for constant variables we ar using const keyword
4. variable names should be comprehendsive and descriptive.
5. do not use spaces on the declared variables
6. all variable names should be unique

 */

// Diffirent casing styles
//  camel case - thisIsCamelCasing
//  snake case - this_is_snake_casing
//  kebab case - this-is-kebab-casing

// this is a good variable name
let firstName = "Michael";

// bad variable name
let pokemon = 2500;

// Declaring and initializing variables
//  Syntax -> let/const variableName = value;

let productName = "Desktop Computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// re-assigning a value top variable
productPrice = 25000;
console.log(productPrice);

let friend = "kate";
friend = "jane";

let supplier;
supplier = "John Smith Tradings";
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// let vs const
//  let variables values can be changed, we can re-assign new values

// const variables values cannot be change

const hoursInAday = 24;
console.log(hoursInAday);

const pie = 3.14;
console.log(pie);

// local/global scope variable

let outerVariable = "Hi"; //global variable

{
    let innerVariable = "hello again"; //This local variable
    console.log(innerVariable);
}
// console.log(innerVariable); this  will throw error innervariable is not accessible since it is enclosed with curly braces

// multiple variable declaration

// Multiple variables may be declared in one line
// its convenient and it is easier to read the code

/* let productCode = "DC017";
let productBrand = "Dell"; */
let productCode = "DC017",
    productBrand = "Dell";
console.log(productCode, productBrand);
// let is a reserve keyword
// const let = "hello";
// console.log(let);

// [SECTION] Data types;

// Strings - are series of characters that creats a word

let country = "Philippines";
let province = "Metro Manila";

// concatenating strings using + symbol
// Combining string values

let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + fullAddress;
console.log(greeting);

// escape character

// "\n" refers to creating a new line between text

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John's Employees went home early";

console.log(message);

message = "John's employees went home early";
console.log(message);

// Numbers
// Integers or whole number

let headcount = 26;
console.log(headcount);

// deciaml or fractions

let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// combining text and strings\
console.log("John's grade lasy quarter is ", grade);

// boolean
// We have 2 boolean values , true and false

let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Arrays
// Are special kind of data type
// used to store multiple values with the same data types

// syntax -> let/const arrayName = [elementA,elementB,....]

let grades = [98.7, 92.1, 90.2];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// holds properties that describes the variable
// syntax -> let/const objectName = {propertyA: Value, propertyB: value}

let person = {
    fullName: "Juan Dela Cruz",
    age: 35,
    contact: ["09123456", "091223211"],
    address: {
        houseNumber: "345",
        city: "Manila",
    },
};

let myGrades = {
    firstGrading: 98.7,
    secondGrading: 92.1,
    thirdGrading: 90.2,
    fourthGrading: 94.6,
};
console.log(myGrades);

// Checking of data type
console.log(typeof grades);
console.log(typeof person);
console.log(typeof isMarried);

// in programming we always start counting from 0
const anime = ["one piece", "one punch man", "attack on titan"];
// anime = ["kimetsu no yaiba"];

anime[0] = "kimetsu no yaiba";
console.log(anime);

// null vs undefined

let spouse = null;
